#!/usr/bin/perl

# Este programa é um software livre; você pode redistribui-lo e/ou 
# modificá-lo dentro dos termos da Licença Pública Geral GNU como 
# publicada pela Fundação do Software Livre (FSF); na versão 2 da 
# Licença, ou (na sua opnião) qualquer versão.
# Este programa é distribuído na esperança que possa ser útil, 
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO a qualquer
# MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, escreva para a Fundação do Software
# Livre(FSF) Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# Ultima atualizacao: 02/07/2012

# Exemplos de uso
# perl delimitador_fronteiras.pl -e in/pt.txt -l pt -n pt -a Abrev.txt -s out/pt_com_tags.txt
# perl delimitador_fronteiras.pl -e in/en.txt -a Abrev.txt -s out/en_without_tags.txt -st

# Programa: delimitador_fronteiras.pl
# Autor: Helena Caseli (LaLiC)
# Funcao: Realiza as etapas de pre-processamento necessarias antes de etiquetar
# ou alinhar sentencialmente ou lexicalmente os textos de um corpus. Sao elas: 
#   1. Identificacao de sentenças
#   2. Etiquetacao de fronteiras de texto (p.ex., <text>), paragrafo (p.ex., <p>), sentenca 
#      (p.ex., <s>)
# Entrada: 
#   1. arquivo a ser processado
#   2. codigo do idioma no qual o arquivo esta escrito
#   3. nome do arquivo
#   (opcionalmente) 4. lista de abreviacoes
# Saida: arquivo de entrada com delimitacao de fronteiras de texto, paragrafos e sentencas
# IMPORTANTE: Para as ferramentas que utilizam o processamento desse script eh melhor precisao
# do que cobertura, ou seja, melhor delimitar menos sentencas, mas com maior certeza do que
# delimitar varias que nao sao realmente sentencas. Por isso, por padrao, a fronteira so
# eh inserida em casos que se tem certeza de que eh mesmo fronteira.

use warnings;
use strict;
use locale;

use Getopt::Long;
use Pod::Usage;
use PorTAl::Aux;

# Parametros de entrada
my($entrada,$lingua,$nome,$saida,$abrev,$verbose,$notag,$help);

$notag = 0;

	GetOptions( 'entrada|e=s' => \$entrada,
					'saida|s=s' => \$saida,
					'lingua|l=s' => \$lingua,
					'nome|n=s' => \$nome,
					'abrev|a=s' => \$abrev,
					'verbose|v' => \$verbose,
          'sem-etiquetas|st' => \$notag,
					'help|h|?' => \$help,) 
		  || pod2usage(2);

	pod2usage(2) if $help;
	pod2usage(2) unless $entrada && $saida && ($notag || ($lingua && $nome));

if ($verbose) { 
	print "\n\tPARAMETROS (PARAMETERS):\n\tEntrada (Input): $entrada\n\tSaida (Output): $saida\n";
	print "\tNome (Name): $nome\n\tIdioma (Language): $lingua\n";  
	if (defined($abrev)) {
		print "\tArquivo com abreviacoes (Abbreviation file): $abrev\n"; 
	}
}

# Le abreviacoes
my %abrevs = ();
if (defined($abrev)) {
	le_abreviacoes($abrev,\%abrevs);
}

# Processa arquivo de entrada gerando arquivo de saida
if ($verbose) {
	print "\n\tProcessando (Processing) ";
}

my($qtds,$qtdp) = 0;

open(IN,"$entrada") or erro(2,$entrada);
open(OUT,">$saida") or erro(2,$saida);

if (!$notag) { print OUT "<text lang=\"$lingua\" id=\"$nome\">\n"; }

while (<IN>) {
	if (/\w/) {
    if ($notag) {
      print OUT divide_sentencas($_,\%abrevs,\$qtds,$notag),"\n";
    }
    else {
  		print OUT "<p>",divide_sentencas($_,\%abrevs,\$qtds,$notag),"\n</p>\n";
    }
		$qtdp++;
	}
	if ($verbose) { print "."; }
}
if (!$notag) { print OUT "</text>\n"; }
close OUT;
close IN;

if ($verbose) { 
	print "\n\n\tFim (The end): $qtdp paragrafos (paragraphs) e (and) $qtds sentencas (sentences)\n\n"; 
#	if ($qtdp != $qtds) { print "Qtd sentencas diferente de Qtd paragrafos - VER $entrada\n"; }
}

# Sub divide_sentencas
# So insere o delimitador se sentencas nos seguintes casos:
# DELIMITADOR = . ! ?
# 1. apos DELIMITADOR seguido de mudanca de linha (\n)
# 2. apos DELIMITADOR seguido de espaco ( ) e letra maiuscula ([A-Z])
# 3. apos DELIMITADOR seguido de espaco ( ) vem aspas (" e ') e letra maiuscula ([A-Z]). Ex: . "A ou . 'O
# 4. apos DELIMITADOR seguido de espaco ( ) vem inicio de sentenca interrogativa (¿) 
# ou exclamativa (¡) em es e letra maiuscula ([A-Z]).                          Ex: . ¿A ou . ¡O
# 5. apos DELIMITADOR seguido de espaco ( ) vem aspas (" e ') e inicio de sentenca interrogativa (¿) 
# ou exclamativa (¡) em es e letra maiuscula ([A-Z]).                          Ex: . "¿A ou . '¡O
# Contanto que o DELIMITADOR nao seja o '.' que segue uma abreviacao Ex: Sr. Bento
sub divide_sentencas {
	my($str,$abrevs,$qtd,$notag) = @_;

	$str =~ s/^\s+//g;	# remove espacos no inicio
	$str =~ s/\s+$//g;	# remove espacos no fim
	my @chars = split(//,$str);

	if ($#chars < 0) { # string vazia
		return "";
	}

	my $output = "<s>";
	my $cabrev = "";
	my $delimita = 0;

	while ($#chars >= 0) {
		if ((($chars[0] =~ /[.!?]/) && ($#chars > 0) && ($chars[1] eq "\n")) 		# Caso 1
			|| (($chars[0] =~ /[.!?]/) && ($#chars > 1) &&  ($chars[1] eq " ") && (maiuscula($chars[2]))) # Caso 2
			|| (($chars[0] =~ /[.!?]/) && ($#chars > 2) &&  ($chars[1] eq " ") && ($chars[2] =~ /["']/) && 
             (maiuscula($chars[3]))) # Caso 3
			|| (($chars[0] =~ /[.!?]/) && ($#chars > 2) &&  ($chars[1] eq " ") && ($chars[2] =~ /[¿¡]/) && 
             (maiuscula($chars[3]))) # Caso 4
			|| (($chars[0] =~ /[.!?]/) && ($#chars > 3) &&  ($chars[1] eq " ") && ($chars[2] =~ /["']/) && 
             ($chars[3] =~ /[¿¡]/) && (maiuscula($chars[4])))) { # Caso 5
			# Antes de inserir um delimitador de sentenca, eh preciso verificar se o que vem antes nao eh uma abreviatura
			if (($chars[0] ne ".") || (!pertence($cabrev,$abrevs))) {
				$delimita = 1;
			}
		}
		if ($delimita) {
			$output .= shift(@chars)."</s>";
			$$qtd++;
			while (($#chars >= 0) && ($chars[0] =~ /\s/)) { shift(@chars); } # remove espaco no inicio da sentenca
			if ($#chars >= 0) { $output .= "<s>"; }
			$delimita = 0;
		}
		else { 
			# Atualiza candidata a abreviatura
			if ($chars[0] eq " ") { $cabrev = ""; }
			else { $cabrev .= $chars[0]; }

			$output .= shift(@chars); 
		}
	}
	if ($output !~ /<\/s>$/) { # casos em que a sentenca eh a unica do paragrafo e nao termina com pontuacao
		$output .= "</s>";
		$$qtd++;
	}

  if ($notag) {
    $output =~ s/<s>//g;
    $output =~ s/<\/s>/\n/g;
  }  
	return $output;
}

# Le a lista de abreviacoes, removendo o '.' ao final, e as insere em $hash
sub le_abreviacoes {
	my($arq,$hash) = @_;

	open(IN,"$arq") or erro(2,$arq);
	while (<IN>) {
		s/\.\s*$//;
		$$hash{$_} = 0;
	}
	close IN;	
}

# Retorna 1 se o elemento ($elemento) pertence ah lista ($hash) e 0 caso contrario
sub pertence {
	my($elemento,$hash) = @_;
	return defined($$hash{$elemento});
}

# Retorna 1 se o caractere eh uma letra maiuscula e 0 caso contrario
sub maiuscula {
	my($c) = @_;

	return ($c =~ /[A-ZÁÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃẼĨÕŨÄËÏÖÜ]/);
}


__END__

=head1 NAME

delimitador_fronteiras 
- Insere etiquetas delimitadoras de texto, paragrafos e sentencas 
- Inserts tags indicating the boundaries of text, paragraphs and sentences

=head1 SYNOPSIS

delimitador_fronteiras [options...] 

 Opcoes (Options):
-entrada|e          arquivo de entrada (obrigatorio) 
                    input file (required)
-saida|s            arquivo de saida (obrigatorio)
                    output file (required)
-nome|n             nome para a etiqueta de identificacao (obrigatorio)
                    name for the text identification tag (required)
-lingua|l           idioma do texto de entrada (obrigatorio)
                    language of input text (required)
-abrev|a            arquivo com abreviacoes comuns no idioma do texto de entrada (opcional)
                    file with common abbreviations in the language of input text (optional)
-sem-etiquetas|st   sem tags (default=com etiquetas)
                    without tags (default=with tags)
-verbose|v          verbose   
-help|h|?           esse guia
                    this guide

Exemplo (Example):

  perl delimitador_fronteiras.pl -e in/pt.txt -l pt -n pt -a Abrev.txt -s out/pt_com_tags.txt

  perl delimitador_fronteiras.pl -e in/en.txt -a Abrev.txt -s out/en_without_tags.txt -st

Copyright (C) 2010-2012  PorTAl (www.lalic.dc.ufscar.br/portal)

=cut

