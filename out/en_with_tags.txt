<text lang="en" id="en">
<p><s>The teeth of the oldest orangutan</s>
</p>
<p><s>A new species of hominid found in Thailand, with an estimated age of 12 million years, has become the most distant relative of today's orangutans ( Pongo pygmaeus ).</s><s>A group of French researchers connected with the European Synchrotron Radiation Facility (ESRF) arrived at this conclusion by comparing the 18 teeth of the fossil with the dentition of other ancient primates.</s>
</p>
<p><s>By means of a technique called microtomography, they made three-dimensional models of the structure of each tooth and of the lower jawbone of the male and of the female of the new species, baptized as Lufengpithecus chiangmuanensis , with a resolution of 1 millionth of a meter.</s>
</p>
<p><s>"We will never be certain that we are dealing with a direct ancestor, but it is something very close", says Jean-Jacques Jaeger, a paleontologist from the University of Montpellier, in France, and one of the authors of this work, published in Nature .</s><s>The hypothesis gains strength from the fact that there was fossilized pollen together with the teeth, suggesting that the new species lived in a tropical forest, just like today's orangutans.</s>
</p>
</text>
